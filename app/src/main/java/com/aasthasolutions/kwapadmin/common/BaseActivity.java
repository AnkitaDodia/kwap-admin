package com.aasthasolutions.kwapadmin.common;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;


import com.aasthasolutions.kwapadmin.R;

import java.text.ParseException;

/**
 * Created by My 7 on 05-Mar-18.
 */

public class BaseActivity extends AppCompatActivity
{
    private Typeface font;

    private static final String TAG = "BaseActivity";


    ProgressDialog mProgressDialog;


    // Code for get and set login
    public void setLogin(int i)
    {
        SharedPreferences sp = getSharedPreferences("LOGIN",MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.putInt("Login",i);
        spe.apply();
    }

    public int getLogin()
    {
        SharedPreferences sp = getSharedPreferences("LOGIN",MODE_PRIVATE);
        int i = sp.getInt("Login",0);
        return i;
    }

    public Typeface getLightFonts()
    {
        font = Typeface.createFromAsset(getAssets(), "fonts/Poppins-Light.ttf");
        return font;
    }

    public Typeface getMediumFonts()
    {
        font = Typeface.createFromAsset(getAssets(), "fonts/Poppins-Medium.ttf");
        return font;
    }

    public Typeface getBoldFonts()
    {
        font = Typeface.createFromAsset(getAssets(), "fonts/Poppins-Bold.ttf");
        return font;
    }

//    public void overrideFonts(final View v) {
//        try {
//            if (v instanceof ViewGroup) {
//                ViewGroup vg = (ViewGroup) v;
//                for (int i = 0; i < vg.getChildCount(); i++) {
//                    View child = vg.getChildAt(i);
//                    overrideFonts(child);
//                }
//            } else if (v instanceof TextView) {
//                ((TextView) v).setTypeface(getTypeFace());
//            }else if (v instanceof Button) {
//                ((Button) v).setTypeface(getTypeFace());
//            }else if (v instanceof EditText) {
//                ((EditText) v).setTypeface(getTypeFace());
//            }
//        } catch (Exception e) {
//        }
//    }

    public static String getStyledFont(String html) {
        boolean addBodyStart = !html.toLowerCase().contains("<body>");
        boolean addBodyEnd = !html.toLowerCase().contains("</body");
        return "<style type=\"text/css\">@font-face {font-family: CustomFont;" +
                "src: url(\"file:///android_asset/fonts/Poppins-Light.ttf\")}" +
                "body {font-family: CustomFont;font-size: small;text-align: justify;}</style>" +
                (addBodyStart ? "<body>" : "") + html + (addBodyEnd ? "</body>" : "");
    }



    public void showWaitIndicator(boolean state) {
        showWaitIndicator(state, "");
    }

    public void showWaitIndicator(boolean state, String message) {
        try {
            try {

                if (state) {

                    mProgressDialog = new ProgressDialog(this, R.style.TransparentProgressDialog);
                    mProgressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Large);
                    mProgressDialog.setCancelable(false);
                    mProgressDialog.show();
                } else {
                    mProgressDialog.dismiss();
                }

            } catch (Exception e) {

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public String getFCMID() {
        SharedPreferences sp = getSharedPreferences("FCM", MODE_PRIVATE);
        String flag = sp.getString("FCM_ID", "");
        return flag;
    }



//    public String getIMEINumber() {
//        TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//        String IMEINumber = mngr.getDeviceId();
//        return IMEINumber;
//    }



    /**
     * Requests given permission.
     * If the permission has been denied previously, a Dialog will prompt the user to grant the
     * permission, otherwise it is requested directly.
     */
    protected void requestPermission(final String permission, String rationale, final int requestCode) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
            showAlertDialog(getString(R.string.permission_title_rationale), rationale,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(BaseActivity.this,
                                    new String[]{permission}, requestCode);
                        }
                    }, getString(R.string.label_ok), null, getString(R.string.label_cancel));
        } else {
            ActivityCompat.requestPermissions(this, new String[]{permission}, requestCode);
        }
    }
    /**
     * This method shows dialog with given title & message.
     * Also there is an option to pass onClickListener for positive & negative button.
     *
     * @param title                         - dialog title
     * @param message                       - dialog message
     * @param onPositiveButtonClickListener - listener for positive button
     * @param positiveText                  - positive button text
     * @param onNegativeButtonClickListener - listener for negative button
     * @param negativeText                  - negative button text
     */
    protected void showAlertDialog(@Nullable String title, @Nullable String message,
                                   @Nullable DialogInterface.OnClickListener onPositiveButtonClickListener,
                                   @NonNull String positiveText,
                                   @Nullable DialogInterface.OnClickListener onNegativeButtonClickListener,
                                   @NonNull String negativeText) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(positiveText, onPositiveButtonClickListener);
        builder.setNegativeButton(negativeText, onNegativeButtonClickListener);
        AlertDialog mAlertDialog = builder.show();
    }
}
