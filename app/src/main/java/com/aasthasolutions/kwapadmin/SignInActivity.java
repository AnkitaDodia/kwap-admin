package com.aasthasolutions.kwapadmin;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aasthasolutions.kwapadmin.common.BaseActivity;
import com.aasthasolutions.kwapadmin.model.Login;
import com.aasthasolutions.kwapadmin.restinterface.RestInterface;
import com.aasthasolutions.kwapadmin.utility.InternetStatus;
import com.google.gson.Gson;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SignInActivity extends BaseActivity {

    SignInActivity mContext;

    boolean isInternet;

    EditText edt_youremail, edt_password;

    TextView txt_toolbar_title;

    Button btn_signin;

    LinearLayout ll_parent_signin;

    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        mContext = this;

        isInternet = new InternetStatus().isInternetOn(mContext);

        setView();
        SetListener();
        SetFonts();
    }

    private void setView() {
        ll_parent_signin = findViewById(R.id.ll_parent_signin);

        txt_toolbar_title = findViewById(R.id.txt_toolbar_title);

        edt_youremail = findViewById(R.id.edt_youremail);
        edt_password = findViewById(R.id.edt_password);

        btn_signin = findViewById(R.id.btn_signin);
    }

    private void SetFonts() {
        txt_toolbar_title.setTypeface(getLightFonts());

        edt_youremail.setTypeface(getLightFonts());
        edt_password.setTypeface(getLightFonts());

        btn_signin.setTypeface(getLightFonts());
    }

    private void SetListener() {

        btn_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_youremail.getText().toString().length() == 0 || edt_password.getText().toString().length() == 0) {
                    Toast.makeText(mContext, "Please enter email address and password!!", Toast.LENGTH_LONG).show();
                } else if (!isValidEmail(edt_youremail.getText().toString())) {
                    Toast.makeText(mContext, "Please enter valid email address!!", Toast.LENGTH_LONG).show();
                } else {
                    if (isInternet) {
                        sendLoginRequest();
                    } else {
                        Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void sendLoginRequest() {
        try {
            String email = edt_youremail.getText().toString();
            String password = edt_password.getText().toString();

            showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            restInterface.sendLoginRequest(email, password, new Callback<Login>() {
                @Override
                public void success(Login model, Response response) {
                    showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("LOGIN_RESPONSE", "" + new Gson().toJson(model));

                            //if successfully login then status 1 else 0
                            if (model.getStatus() == 1) {
                                setLogin(1);

                                Toast.makeText(mContext, model.getMessage(), Toast.LENGTH_LONG).show();

                                Intent i = new Intent(mContext, LaunchScannerActivity.class);
                                startActivity(i);
                                finish();
                            } else {
                                Toast.makeText(mContext, model.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    showWaitIndicator(false);
                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
}
