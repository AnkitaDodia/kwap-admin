package com.aasthasolutions.kwapadmin;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aasthasolutions.kwapadmin.common.BaseActivity;
import com.aasthasolutions.kwapadmin.model.QREmail;
import com.aasthasolutions.kwapadmin.restinterface.RestInterface;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.gson.Gson;
import com.mikhaellopez.circularimageview.CircularImageView;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class LaunchScannerActivity extends BaseActivity {

    Context mContext;
    private static final int RC_BARCODE_CAPTURE = 9001;
    private static final String TAG = "BarcodeMain";
    Button btn_launchscanner;

    private String beepSoundFile;
    TextView txt_toolbar_title, txt_logout;
    LinearLayout layout_logout;

    //For Dialog
    TextView txt_participants_details,txt_full_name_title,txt_full_name,txt_company_title,txt_company,
            txt_email_title,txt_email,txt_msg;
    Button btn_inactive,btn_attendance;

    //For error dialog
    CircularImageView img_close_error;
    Button btn_msg;
    TextView txt_noti;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch_scanner);

        mContext = this;

        btn_launchscanner = findViewById(R.id.btn_launchscanner);
        txt_toolbar_title = findViewById(R.id.txt_toolbar_title);
        txt_logout = findViewById(R.id.txt_logout);
        layout_logout = findViewById(R.id.layout_logout);

        txt_logout.setTypeface(getMediumFonts());
        txt_toolbar_title.setTypeface(getMediumFonts());
        btn_launchscanner.setTypeface(getLightFonts());

        btn_launchscanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // launch barcode activity.
                Intent intent = new Intent(mContext, BarcodeCaptureActivity.class);
                intent.putExtra(BarcodeCaptureActivity.AutoFocus, true );//autoFocus.isChecked()
                intent.putExtra(BarcodeCaptureActivity.UseFlash, false);//useFlash.isChecked()

                startActivityForResult(intent, RC_BARCODE_CAPTURE);

            }
        });

        layout_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setLogin(0);
                Intent is = new Intent(mContext, SignInActivity.class);
                startActivity(is);
                finish();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_BARCODE_CAPTURE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);

                    Log.e(TAG, "Barcode read: " + barcode.displayValue);

                    playBeep();
                    sendQREmailRequest(GetEmailId(barcode.displayValue));

                } else {

                    Log.d(TAG, "No barcode captured, intent data is null");
                }
            }
        }
        else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private String GetEmailId(String example){
        return example.substring(example.lastIndexOf(":") + 1).trim();
    }

    public void playBeep() {
        MediaPlayer m = new MediaPlayer();
        try {
            if (m.isPlaying()) {
                m.stop();
                m.release();
                m = new MediaPlayer();
            }

            AssetFileDescriptor descriptor = mContext.getAssets().openFd(beepSoundFile != null ? beepSoundFile : "beep.mp3");
            m.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();

            m.prepare();
            m.setVolume(1f, 1f);
            m.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendQREmailRequest(String email) {
        try {
            showWaitIndicator(true);

            RestAdapter adapter = new RestAdapter.Builder().setEndpoint(RestInterface.API_BASE_URL).build();

            RestInterface restInterface = adapter.create(RestInterface.class);

            restInterface.sendQREmailRequest(email, new Callback<QREmail>() {
                @Override
                public void success(QREmail model, Response response) {
                    showWaitIndicator(false);

                    if (response.getStatus() == 200) {
                        try {
                            Log.e("QREMAIL_RESPONSE", "" + new Gson().toJson(model));

                            if (model.getStatus() == 0 || model.getStatus() == 1 || model.getStatus() == 2)
                            {
                                String fullName = model.getName();
                                String company = model.getCompany();
                                String email = model.getEmail();
                                String msg = model.getMessage();

                                displayDialog(fullName,company,email,msg,model.getStatus());
                            }


                            /*if (model.getStatus() == 1 || model.getStatus() == 2) {
                                String fullName = model.getName();
                                String company = model.getCompany();
                                String email = model.getEmail();
                                String msg = model.getMessage();

                                displayDialog(fullName,company,email,msg,model.getStatus());
                            } else {
                                showErrorDialog(model.getMessage());
                            }*/
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    showWaitIndicator(false);
                    Log.e("ERROR", "" + error.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showErrorDialog(String message) {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(mContext);
        View mView = getLayoutInflater().inflate(R.layout.error_dialog, null);

        img_close_error = mView.findViewById(R.id.img_close_error);

        txt_noti = mView.findViewById(R.id.txt_noti);

        btn_msg = mView.findViewById(R.id.btn_msg);

        txt_noti.setTypeface(getBoldFonts());
        btn_msg.setTypeface(getBoldFonts());

        btn_msg.setText(message);

        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.show();

        img_close_error.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    private void displayDialog(String fullName, String company, String email,String msg,int status)
    {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(mContext);
        View mView = getLayoutInflater().inflate(R.layout.dialog, null);
        final CircularImageView img_close = mView.findViewById(R.id.img_close);

        txt_participants_details = mView.findViewById(R.id.txt_participants_details);
        txt_full_name_title = mView.findViewById(R.id.txt_full_name_title);
        txt_full_name = mView.findViewById(R.id.txt_full_name);
        txt_company_title = mView.findViewById(R.id.txt_company_title);
        txt_company = mView.findViewById(R.id.txt_company);
        txt_email_title = mView.findViewById(R.id.txt_email_title);
        txt_email = mView.findViewById(R.id.txt_email);
        txt_msg = mView.findViewById(R.id.txt_msg);

        btn_inactive = mView.findViewById(R.id.btn_inactive);
        btn_attendance = mView.findViewById(R.id.btn_attendance);

        txt_participants_details.setTypeface(getBoldFonts());
        txt_full_name_title.setTypeface(getBoldFonts());
        txt_full_name.setTypeface(getBoldFonts());
        txt_company_title.setTypeface(getBoldFonts());
        txt_company.setTypeface(getBoldFonts());
        txt_email_title.setTypeface(getBoldFonts());
        txt_email.setTypeface(getBoldFonts());
        txt_msg.setTypeface(getBoldFonts());
        btn_inactive.setTypeface(getBoldFonts());
        btn_attendance.setTypeface(getBoldFonts());

        if(status == 1)
        {
            txt_participants_details.setTextColor(getResources().getColor(R.color.dialog_title));
            txt_participants_details.setText("PARTICIPANTS DETAILS");
            btn_inactive.setVisibility(View.GONE);
            txt_msg.setVisibility(View.GONE);
            btn_attendance.setText(msg);
            btn_attendance.setBackground(getResources().getDrawable(R.drawable.black_button_selector));
        }
        else if(status == 0)
        {
            txt_participants_details.setTextColor(getResources().getColor(R.color.dialog_notification));
            txt_participants_details.setText("NOTIFICATION");
            btn_inactive.setVisibility(View.GONE);
            txt_msg.setVisibility(View.GONE);
            btn_attendance.setText(msg);
            btn_attendance.setBackground(getResources().getDrawable(R.drawable.button_black_red_normal));
        }
        else
        {
            txt_participants_details.setTextColor(getResources().getColor(R.color.dialog_title));
            txt_participants_details.setText("PARTICIPANTS DETAILS");
            btn_inactive.setVisibility(View.VISIBLE);
            txt_msg.setVisibility(View.VISIBLE);
            btn_attendance.setText(msg);
            btn_attendance.setBackground(getResources().getDrawable(R.drawable.black_button_selector));
        }

        if(fullName != null)
        {
            txt_full_name.setVisibility(View.VISIBLE);
            txt_full_name.setText(fullName);
            txt_full_name_title.setVisibility(View.VISIBLE);
        }
        else
        {
            txt_full_name.setVisibility(View.GONE);
            txt_full_name_title.setVisibility(View.GONE);
        }

        if(company != null)
        {
            txt_company.setVisibility(View.VISIBLE);
            txt_company.setText(company);
            txt_company_title.setVisibility(View.VISIBLE);
        }
        else
        {
            txt_company.setVisibility(View.GONE);
            txt_company_title.setVisibility(View.GONE);
        }

        if(email != null)
        {
            txt_email.setVisibility(View.VISIBLE);
            txt_email.setText(email);
            txt_email_title.setVisibility(View.VISIBLE);
        }
        else
        {
            txt_email.setVisibility(View.GONE);
            txt_email_title.setVisibility(View.GONE);
        }


        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.show();

        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }
}
