package com.aasthasolutions.kwapadmin.restinterface;


import com.aasthasolutions.kwapadmin.model.Login;
import com.aasthasolutions.kwapadmin.model.QREmail;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by codfidea on 6/30/2016.
 */
public interface RestInterface {
    public static String API_BASE_URL = "http://app.kwapinspire.com.my";

    String LOGIN = "/APIUser/adminlogin";
    String QREmail = "/APIUser/qrCounter";

    //LogIn
    @FormUrlEncoded
    @POST(LOGIN)
    public void sendLoginRequest(@Field("email") String email, @Field("password") String password, Callback<Login> callBack);


    //LogIn
    @FormUrlEncoded
    @POST(QREmail)
    public void sendQREmailRequest(@Field("email") String email, Callback<QREmail> callBack);

}
