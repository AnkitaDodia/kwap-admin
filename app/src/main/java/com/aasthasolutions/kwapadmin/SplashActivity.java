package com.aasthasolutions.kwapadmin;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.aasthasolutions.kwapadmin.common.BaseActivity;

public class SplashActivity extends BaseActivity {

    private static int SPLASH_TIME_OUT = 3000;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mContext = this;

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

//                Intent i = new Intent(mContext, LaunchScannerActivity.class);
//                startActivity(i);
//                finish();


               if(getLogin() == 1)
                {
                    Intent il = new Intent(mContext, LaunchScannerActivity.class);
                    startActivity(il);
                    finish();
                }
                else
                {
                    Intent is = new Intent(mContext, SignInActivity.class);
                    startActivity(is);
                    finish();
                }

            }
        }, SPLASH_TIME_OUT);
    }
}
